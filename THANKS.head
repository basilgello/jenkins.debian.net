== Thanks

Many many thanks to our sponsors and contributors! Without you, jenkins.debian.net would not exist today!

=== Sponsors

link:https://jenkins.debian.net/["jenkins.debian.net"] would not be possible without our sponsors:

 * Since October *2012* jenkins.debian.net has been running on (virtualized) hardware sponsored by link:https://www.ionos.com[IONOS] (formerly known as Profitbricks) - *&lt;blink&gt;Thank you very very very much!&lt;/blink&gt;*!
 * Currently we are using:
 ** 23 cores and 75 GB memory for jenkins.debian.net
 ** 15 cores and 48 GB memory for ionos1-amd64.debian.net used for building amd64 Debian packages for tests.reproducible-builds.org (t.r-b.o)
 ** 10 cores and 8 GB memory for ionos2-i386.debian.net used for building i386 Debian packages for t.r-b.o
 ** 16 cores and 48 GB memory for ionos5-amd64.debian.net used for building amd64 Debian packages for t.r-b.o, running in the future
 ** 9 cores and 8 GB memory for ionos6-i386.debian.net used for building i386 Debian packages for t.r-b.o, running in the future
 ** 15 cores and 48 GB memory for ionos11-amd64.debian.net used for building amd64 Debian packages for tests.reproducible-builds.org (t.r-b.o)
 ** 10 cores and 8 GB memory for ionos12-i386.debian.net used for building i386 Debian packages for t.r-b.o
 ** 16 cores and 48 GB memory for ionos15-amd64.debian.net used for building amd64 Debian packages for t.r-b.o, running in the future
 ** 9 cores and 8 GB memory for ionos16-i386.debian.net sed for building i386 Debian packages for t.r-b.o, running in the future
 ** 2 cores and 4 GB memory for ionos3-amd64.debian.net used for our twitterbot.
 ** 2 cores and 8 GB memory for ionos7-amd64.debian.net used for buildinfos.debian.net
 ** 5 cores and 10 GB memory for ionos9-amd64.debian.net used for rebootstrap jobs
 ** 4 cores and 12 GB memory for ionos10-amd64.debian.net used for chroot-installation jobs
 ** 9 cores and 19 GB memory for freebsd-jenkins.debian.net (also running on IONOS virtual hardware), used for building FreeBSD for t.r-b.o
 * link:https://qa.debian.org/developer.php?login=vagrant%40debian.org[Vagrant] provides and hosts 13 'armhf' systems, used for building armhf Debian packages for t.r-b.o:
 ** four quad-cores with 4 GB RAM each,
 * servers provided by linaro for 'armhf':
 ** one octo-core with 32GB of ram (divided into two virtual machines running at ~15GB ram each)
 ** two octo-core with 16GB of ram (divided into four virtual machines running at ~7GB ram each)
 * In December 2016 link:https://codethink.co.uk[Codethink] has kindly offered access to 8 arm64 build nodes on link:http://www8.hp.com/us/en/hp-news/press-release.html?id=1800094#.VgQdelox1QI[HP moonshot hardware].
 ** 8 cores and 64 GB memory for codethink9-arm64.debian.net used for building arm64 Debian packages for t.r-b.o, running in the future
 ** 8 cores and 64 GB memory for codethink10-arm64.debian.net used for building arm64 Debian packages for t.r-b.o
 ** 8 cores and 64 GB memory for codethink11-arm64.debian.net used for building arm64 Debian packages for t.r-b.o, running in the future
 ** 8 cores and 64 GB memory for codethink12-arm64.debian.net used for building arm64 Debian packages for t.r-b.o
 ** 8 cores and 64 GB memory for codethink13-arm64.debian.net used for building arm64 Debian packages for t.r-b.o, running in the future
 ** 8 cores and 64 GB memory for codethink14-arm64.debian.net used for building arm64 Debian packages for t.r-b.o
 ** 8 cores and 64 GB memory for codethink15-arm64.debian.net used for building arm64 Debian packages for t.r-b.o, running in the future
 ** 8 cores and 64 GB memory for codethink16-arm64.debian.net used for building arm64 Debian packages for t.r-b.o
 * link:https://letsencrypt.org[Let's encrypt] provides free of charge SSL certificates for jenkins.debian.net, reproducible.debian.net and tests.reproducible-builds.org.
 * In December 2018 we were given access to eight nodes which were donated by Facebook to the GCC Compile Farm project and are now hosted by link:https://osuosl.org/[OSUOSL]:
  ** 32 cores with 144 GB memory for osuosl167-amd64.debian.net
  ** 32 cores with 144 GB memory for osuosl168-amd64.debian.net used for building F-Droid for t.r-b.o
  ** 32 cores with 144 GB memory for osuosl169-amd64.debian.net used for building Arch Linux for t.r-b.o
  ** 32 cores with 144 GB memory for osuosl170-amd64.debian.net used for building Arch Linux for t.r-b.o
  ** 32 cores with 144 GB memory for osuosl171-amd64.debian.net used for building OpenWrt, coreboot and NetBSD for t.r-b.o
  ** 32 cores with 144 GB memory for osuosl172-amd64.debian.net used for building OpenWrt, coreboot for t.r-b.o
  ** 32 cores with 144 GB memory for osuosl173-amd64.debian.net
  ** 32 cores with 144 GB memory for osuosl174-amd64.debian.net

==== Past sponsors

 * link:https://globalsign.com[GlobalSign] from January 2015 to January 2016 provided free of charge SSL certificates for both jenkins.debian.net and reproducible.debian.net.

=== Contributors

link:https://jenkins.debian.net/["jenkins.debian.net"] would not be possible without these contributors:


----

