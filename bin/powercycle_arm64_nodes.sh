#!/bin/bash
# vim: set noexpandtab:

# Copyright 2019-2022 Holger Levsen <holger@layer-acht.org>
#           2020 Mattia Rizzolo <mattia@debian.org>
# released under the GPLv2

JH=reproducible@jumpserv.colo.codethink.co.uk

# validate input
for i in "$@" ; do
	case $i in
		9|10|11|12|13|14|15|16)
			ssh "$JH" ./sled-power-cycle.sh "$@"
			;;
		*) 	echo 'invalid number for a codethink node, skipping.'
			continue
			;;
	esac
done

exit 0

#
# this is the end
#










# following is the script that lives at jumpserv.colo.codethink.co.uk
# - remember to place the moonshot-ilo password in the "ilopw" file
for i in "$@" ; do
	case "$i" in
		9|10|11|12|13|14|15|16)	: ;;
		*) continue ;;
	esac
	echo Force power off sled "$i"
	sshpass -f ilopw ssh reproducible@moonshot-ilo "SET NODE POWER OFF FORCE C${i}N1"
	echo sleeping 10 seconds
	sleep 10
	echo Power on sled "$i"
	sshpass -f ilopw ssh reproducible@moonshot-ilo "SET NODE POWER ON C${i}N1"
done
