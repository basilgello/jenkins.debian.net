#!/bin/bash

set -eu

OP=$1

_log () {
    echo " + ($OP) $*"
}

reload_apache () {
    _log "Reloading apache..."
    sudo apache2ctl graceful
}

email () {
    # $1: domain name $6: timestamp of cert creation
    printf "%s\n\n    %s\t%s" \
            "The following SSL certifcate has just been renewed:" \
            "$1" "$(date -u -d @"$6")" | \
        mail -s "R-B SSL certifcate renewed" root
}

case "$OP" in
    deploy_cert)
        shift
        reload_apache
        email "$@"
        ;;
    *)
        ;;
esac

